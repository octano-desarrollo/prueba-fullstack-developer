# Prueba Fullstack Developer #

### Lo que se busca ###

* Crear un pequeño sistema de autenticación de usuario 
* Separar un entorno privado de uno público 
* Uso de frameworks requeridos para el proyecto

### Instrucciones y requisitos técnicos ###

* Se deben crear 2 vistas, una pública y otra con contenido visible solo por usuarios autenticados. Adicionalmente se debe incluir la pantalla de login.
* Se deben usar capas separadas para frontend y backend
* El backend será una API que entregará una cierta cantidad de endpoints definidos por el postulante. Las respuestas deben ser en formato JSON, con estados HTTP válidos, uso de verbos HTTP y debe validar las respuestas según autenticación 
* El frontend deberá consumir las respuestas del backend para validar usuarios y habilitar la visibilidad del área privada o denegar el acceso
* Debe ser responsive
* Preferentemente debiese utilizar el siguiente stack de desarrollo 
	* Groovy
	* Grails
	* Creación de API REST
* Sino se tiene conocimiento del stack de desarrollo anterior, también se puede
	* Cualquier FrameWork MVC, idealmente Java de alto nivel (Struts, Spring)
	* Creación de API REST
* Suma puntos si usa gestor de paquetes
* Conocimiento de controlador de versiones
* Recordar enviar e-mail, al finalizar la prueba. Este será el tiempo controlado

### Lo que se debe entregar ###

* Código fuente, y archivos de instalación de dependencias
* Documento Instrucciones de instalación y ejecución 
* Se debe entregar el código lo más ordenado posible, documentando el código que implique una mayor complejidad de lectura

### Rubrica de evaluación ###

* Cumplimiento de los requerimientos técnicos solicitados
* Tiempo de desarrollo, mientras antes es mejor pero al no estar completo se penalizará puntaje
* Utilización correcta del framework
* Diseño Frontend estético y responsive, se puede usar librerías de ayuda
* Documentación dentro del código, junto a documento técnico de instalación y uso.
* Protocolo de seguridad, solo se permite ingresar a página privada al estar registrado
* Uso de controlador de versiones
* Bonus: Gestor de paquetes

### Reglas ###

* La prueba tiene un deadline a las 18:00 del día 11 de Enero
* Finalizado el plazo, el postulante deberá subir su trabajo a un fork de un repositorio GIT a especificar más adelante
* Cada postulante debe crear una rama propia para subir sus archivos
* Los minutos que excedan del deadline restará puntaje